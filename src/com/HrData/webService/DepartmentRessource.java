package com.HrData.webService;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.HrData.models.Department;
import com.HrData.services.CompanyService;
import com.HrData.services.DepartmentService;
import com.google.gson.Gson;
@Path("department")
public class DepartmentRessource {
	@GET
	@Produces("application/json")
	@Path("company_id={company_id}")
	public JSONArray getAll(@PathParam("company_id")int company_id) throws JSONException {
		DepartmentService service = new DepartmentService();

		Gson gson = new Gson();
		
	return new JSONArray(gson.toJson(service.getCompanyDepartments(company_id)));
	}
	@POST
	@Produces("application/json")
	@Path("new")
	@Consumes(MediaType.APPLICATION_JSON)
	public JSONObject createDepartment(JSONObject data) throws JSONException{
	JSONObject	JObject=new JSONObject();
		DepartmentService service = new DepartmentService();
		Department d=new Department();
		d.setNom((String)data.get("name"));
		d.setCompanyId(Integer.parseInt((String) data.get("company_id")));
		service.addDepartment(d);
		JObject.put("id", d.getId());
		JObject.put("nom", d.getNom());
		return JObject;
	
	}
	
	@POST
	@Produces("application/json")
	@Path("remove")
	@Consumes(MediaType.APPLICATION_JSON)
	public JSONObject removeCompany(JSONObject data) throws JSONException{
	JSONObject	JObject=new JSONObject();
		DepartmentService service = new DepartmentService();
		service.removeDepartment((Integer)data.get("id"));
		JObject.put("success", true);
		return JObject;
	
	}

}
