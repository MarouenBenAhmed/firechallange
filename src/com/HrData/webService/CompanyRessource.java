package com.HrData.webService;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.HrData.models.Company;
import com.HrData.services.CompanyService;
import com.google.gson.Gson;

@Path("/company")
public class CompanyRessource {
	@GET
	@Produces("application/json")
	@Path("all")
	public JSONArray getAll() throws JSONException {
		CompanyService service = new CompanyService();

		Gson gson = new Gson();
		
	return new JSONArray(gson.toJson(service.getAllCompanies()));
	}
	@POST
	@Produces("application/json")
	@Path("new")
	@Consumes(MediaType.APPLICATION_JSON)
	public JSONObject createCompany(JSONObject data) throws JSONException{
	JSONObject	JObject=new JSONObject();
		CompanyService service = new CompanyService();
		Company company =new Company();
		company.setName((String) data.get("name"));
		service.addCompany(company);
		JObject.put("id", company.getId());
		JObject.put("name", company.getName());
		return JObject;
	
	}
	@POST
	@Produces("application/json")
	@Path("remove")
	@Consumes(MediaType.APPLICATION_JSON)
	public JSONObject removeCompany(JSONObject data) throws JSONException{
	JSONObject	JObject=new JSONObject();
		CompanyService service = new CompanyService();
		
		service.removeCompany((Integer)data.get("id"));
		JObject.put("success", true);
		return JObject;
	
	}

}
