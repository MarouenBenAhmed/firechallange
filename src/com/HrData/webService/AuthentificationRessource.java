package com.HrData.webService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.google.gson.Gson;
import com.HrData.services.*;

@Path("/autho")
public class AuthentificationRessource {

	@POST
	@Produces("application/json")
	@Path("login")
	@Consumes(MediaType.APPLICATION_JSON)
	public JSONObject getAuthorization(JSONObject inputJsonObj,@Context HttpServletRequest req)
			throws JSONException {
		UserService ser = new UserService();
		HttpSession sec = req.getSession(false);
		JSONObject JObject = new JSONObject();
		
		if (sec != null) {

			String username = (String) sec.getAttribute("username");
			String pass = (String) sec.getAttribute("password");
			JObject.put("success", true);
			JObject.put("uid", sec.getId());
			return JObject;

		} else {
            
			if (ser.verifUser((String) inputJsonObj.get("username"),
					(String) inputJsonObj.get("password"))) {
				// affect a session
				sec = req.getSession(true);
				sec.setAttribute("username",
						(String) inputJsonObj.get("username"));
				sec.setAttribute("password",
						(String) inputJsonObj.get("password"));
				JObject.put("success", true);
				JObject.put("uid", sec.getId());
				return JObject;
			} else {
				
				JObject.put("success", false);
				JObject.put("error_msg", "Check your informations please !!");
				return JObject;
			}

		}

	}

	@POST
	@Produces("application/json")
	@Path("logout")
	public JSONObject getLogOut(@Context HttpServletRequest req)
			throws JSONException {

		if (req.getSession(false) != null) {
			HttpSession sec = req.getSession(false);
			sec.invalidate();

		}

		JSONObject JObject = new JSONObject();
		JObject.put("success", true);
		return JObject;
	}

}