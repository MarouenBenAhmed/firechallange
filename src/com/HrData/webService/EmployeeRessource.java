package com.HrData.webService;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.HrData.models.Employee;
import com.HrData.services.CompanyService;
import com.HrData.services.EmployeService;
import com.google.gson.Gson;

@Path("employee")
public class EmployeeRessource {
	@GET
	@Produces("application/json")
	@Path("department_id={department_id}")
	public JSONArray getAll(@PathParam("department_id") int department_id)
			throws JSONException {
		EmployeService service = new EmployeService();

		Gson gson = new Gson();

		return new JSONArray(gson.toJson(service.getEmployees(department_id)));
	}

	@POST
	@Produces("application/json")
	@Path("new")
	@Consumes(MediaType.APPLICATION_JSON)
	public JSONObject createEmployee(JSONObject data) throws JSONException,
			ParseException {
		JSONObject JObject = new JSONObject();
		EmployeService service = new EmployeService();

		Employee e = new Employee();
		e.setFullName((String) data.get("full_name"));
		e.setCelphone((String) data.get("Celphone"));
		e.setEmail((String) data.get("email"));
		e.setJob((String) data.get("title"));
		e.setPhone((String) data.get("phone"));
		e.setLocation((String) data.get("location"));
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		e.setEndDate(format.parse((String) data.get("end_date").toString()
				.split("T")[0]));
		e.setStartDate(format.parse((String) data.get("start_date").toString()
				.split("T")[0]));
		e.setDepartementId(Integer.parseInt((String) data.get("departement_id")));
		service.createWorker(e);
		JObject.put("success", true);
		return JObject;

	}

	@POST
	@Produces("application/json")
	@Path("remove")
	@Consumes(MediaType.APPLICATION_JSON)
	public JSONObject removeEmploye(JSONObject data) throws JSONException {
		JSONObject JObject = new JSONObject();
		EmployeService service = new EmployeService();
		service.removeEmploye((Integer) data.get("id"));
		JObject.put("success", true);
		return JObject;

	}

}
