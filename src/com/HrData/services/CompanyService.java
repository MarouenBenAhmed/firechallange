package com.HrData.services;

import java.util.List;

import org.hibernate.Session;

import com.HrData.dao.CompanyHome;
import com.HrData.dao.HibernateUtil;
import com.HrData.models.Company;

public class CompanyService {
	private CompanyHome companyDao;

	public CompanyService() {

		this.companyDao = new CompanyHome();
	}

	public List<Company> getAllCompanies() {
		Session sc = HibernateUtil.getSessionFactory().getCurrentSession();
		sc.beginTransaction();
		List<Company> companies=this.companyDao.findAll();
		sc.getTransaction().commit();
		return companies;

	}

	public Company addCompany(Company company) {
		Session sc = HibernateUtil.getSessionFactory().getCurrentSession();
		sc.beginTransaction();
		this.companyDao.persist(company);
		sc.getTransaction().commit();
		return company;
	}

	public void removeCompany(int id) {
		Session sc = HibernateUtil.getSessionFactory().getCurrentSession();
		sc.beginTransaction();
		Company c=this.companyDao.findById(id);
		this.companyDao.delete(c);
		sc.getTransaction().commit();
		
	}

}
