package com.HrData.services;

import java.util.List;

import org.hibernate.classic.Session;

import com.HrData.dao.CompanyHome;
import com.HrData.dao.DepartmentHome;
import com.HrData.dao.HibernateUtil;
import com.HrData.models.Company;
import com.HrData.models.Department;

public class DepartmentService {

	private DepartmentHome departmentDao;
	private CompanyHome companDao;

	public DepartmentService() {

		this.departmentDao = new DepartmentHome();
		this.companDao = new CompanyHome();
	}

	public List<Department> getCompanyDepartments(int company_id) {
		Session sc = HibernateUtil.getSessionFactory().getCurrentSession();
		sc.beginTransaction();
		
		List<Department> departments = this.departmentDao
				.findCompanyDepartments(company_id);
		sc.getTransaction().commit();
		return departments;

	}

	public void addDepartment(Department d) {
		Session sc = HibernateUtil.getSessionFactory().getCurrentSession();
		sc.beginTransaction();
		this.departmentDao.persist(d);
		sc.getTransaction().commit();	
		
	}

	public void removeDepartment(Integer id) {
		// TODO Auto-generated method stub
		Session sc = HibernateUtil.getSessionFactory().getCurrentSession();
		sc.beginTransaction();
		Department d=this.departmentDao.findById(id);
		this.departmentDao.delete(d);
		sc.getTransaction().commit();	
		
		
	}

}
