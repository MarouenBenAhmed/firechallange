package com.HrData.services;

import org.hibernate.classic.Session;

import com.HrData.dao.HibernateUtil;
import com.HrData.dao.UserHome;
import com.HrData.models.User;

public class UserService {

	private UserHome userDao;

	public UserService() {

		this.userDao = new UserHome();
	}

	public boolean verifUser(String username, String password) {
         User user=null;
		Session sc = HibernateUtil.getSessionFactory().getCurrentSession();
		sc.beginTransaction();
        user=this.userDao.findByUserName(username, password);
		sc.getTransaction().commit();
		
       if (user!=null){
    	  return true;
      }
		return false;
	}

}
