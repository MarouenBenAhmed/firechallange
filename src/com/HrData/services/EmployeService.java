package com.HrData.services;

import java.util.List;

import org.hibernate.classic.Session;

import com.HrData.dao.EmployeeHome;
import com.HrData.dao.HibernateUtil;
import com.HrData.models.Department;
import com.HrData.models.Employee;

public class EmployeService {
	
	private EmployeeHome employeDao;
	
	
	 public EmployeService(){
		 
this.employeDao=new EmployeeHome();
	 }


	public List<Employee> getEmployees(int department_id) {
		Session sc = HibernateUtil.getSessionFactory().getCurrentSession();
		sc.beginTransaction();
		
		List<Employee> employees = this.employeDao
				.findDepEmployees(department_id);
		sc.getTransaction().commit();
		return employees;
	}


	public void createWorker(Employee e) {
		Session sc = HibernateUtil.getSessionFactory().getCurrentSession();
		sc.beginTransaction();
		this.employeDao.persist(e);
		sc.getTransaction().commit();
		
	}


	public void removeEmploye(Integer id) {
		Session sc = HibernateUtil.getSessionFactory().getCurrentSession();
		sc.beginTransaction();
		Employee e=this.employeDao.findById(id);
		this.employeDao.delete(e);
		sc.getTransaction().commit();
		
	}
	 
	 

}
