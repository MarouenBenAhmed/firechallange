package com.HrData.dao;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

import com.HrData.models.Company;
import com.HrData.models.Department;
import com.HrData.models.Employee;
import com.HrData.models.User;

public class HibernateUtil {
	private static SessionFactory sessionFactory;
	static {
		try {
			sessionFactory = new AnnotationConfiguration().configure()
					.addAnnotatedClass(User.class)
					.addAnnotatedClass(Company.class)
					.addAnnotatedClass(Department.class)
					.addAnnotatedClass(Employee.class).buildSessionFactory();
		} catch (Throwable ex) {
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public static void shutdown() {
		getSessionFactory().close();
	}
}