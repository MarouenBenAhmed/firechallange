/**
 * This file contains general config and routes of our SPA HR DATA APPLICATION
 *
 *
 */
(function(){
    var app=angular.module("HrData",['ngRoute']);
    /**
     * app.config make us able to config out routing using the given module By ANGULAR JS
     * NAMED ngRoute
     *
     *
     */

    app.config(['$routeProvider',
        function($routeProvider) {
            $routeProvider.when(
                '/',{
                    templateUrl: 'app/views/login.html',
                    controller: 'LoginController'

                }).when("/home",{
                    templateUrl: 'app/views/home.html',
                    controller: 'HomeController'
                })
                .when("/people_directory",{
                    templateUrl: 'app/views/people_directory.html',
                    controller: 'PeopleController'

                }).
                when("/jobs_manager",{
                    templateUrl: 'app/views/jobs_manager.html',
                    controller: 'JobsController'
                }).
                when("/stats",{
                    templateUrl: 'app/views/stats.html',
                    controller: 'StatsController'
                })
                .otherwise({
                    redirectTo: '/'
                });


        }]);

/**
 * app.run while listen to the event linked to
 * load of our Angular js App
 **/
app.run(function($rootScope,$location,LoginService) {
    $rootScope.$on( "$routeChangeStart", function(event, next, current) {
      if(!LoginService.isLogged()){
          $location.path('/');
      }
    });
});


})();
