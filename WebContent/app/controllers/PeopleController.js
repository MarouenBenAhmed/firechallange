/**
 * Created by MBM info on 05/04/2015.
 */
/**
 * Created by MBM info on 05/04/2015.
 */
/**
 * Created by MBM info on 04/04/2015.
 */
(function(){

    var PeopleController=function($scope,LoginService,CompanyService,DepartmentService,EmployeService){
        $scope.companies=[];
        $scope.workers=[];
        $scope.company={
            name:""
        };
        $scope.department={
            name:"",
            company_id:""
        };
        $scope.relativeDeps=[];
        $scope.error_add_company="";
        $scope.error_add_department="";
        $scope.error_add_worker="";
        $scope.departments=[];
        $scope.msgLoadCompanies="";
        $scope.msgLoadDepartments="";
        $scope.selectedCompany=null;
        $scope.selectedDep=null;
        $scope.selectedWorker=null;

        function init(){
            $scope.msgLoadCompanies="Loading........";
            CompanyService.getCompanies().then(function(msg){
                $scope.companies=msg.data;
                $scope.msgLoadCompanies="";
            });

        }
        function refreshData(){
            init();
            $scope.selectedCompany=null;
            $scope.selectedDep=null;
            $scope.selectedWorker=null;
            $scope.departments=[];
            $scope.workers=[];
        }

        init();
        $scope.loadWorker=function(worker){
            $scope.selectedWorker=worker;
        };
        $scope.addWorker=function(worker){
            if($scope.workerForm.$valid){
            EmployeService.createWorker(worker).then(function(msg) {
                    $("#modalAddWorker").modal("hide");
                    $scope.error_add_worker="";
                    toastr.success('New employee has been created with success');
                    refreshData();
                });
            }else{
                $scope.error_add_worker="All fields are required";

            }

        }
        $scope.confDeleteCompany=function(){
            CompanyService.removeCompany($scope.selectedCompany).then(function(msg){

                toastr.success('The company and all relative data has been deleted with success');
                $("#modalDeleteCompany").modal('hide');
                refreshData();
            });

        };
        $scope.confDeleteWorker=function(){
           EmployeService.removeWorker($scope.selectedWorker).then(function(msg){

                toastr.success('The employee has been deleted with success');
                $("#modalDeleteWorker").modal('hide');
                refreshData();
            });

        };
        $scope.confDeleteDepartment=function(){
            DepartmentService.removeDepartment($scope.selectedDep).then(function(msg){
                toastr.success('The department and all relative data has been deleted with success');
                $("#modalDeleteDepartment").modal('hide');
                refreshData();
            });

        };
        $scope.initAddWorker=function(){
            $("#modalAddWorker").modal("show");

        };
        $scope.loadRelativeDeps=function(com){
            DepartmentService.getDepartments(com).then(function(msg) {
                $scope.relativeDeps = msg.data;
            });

        };
        $scope.refresh=function(){
            refreshData();
        };
        $scope.initAddCompany=function(){
            $("#addCompany").modal("show");
            $scope.error_add_company="";
        };
        $scope.initAddDepartment=function(){
            $("#addDepartment").modal("show");
            $scope.error_add_department="";
        };
        $scope.addDepartment=function(department){
            if($scope.formAddDepartment.$valid){
                DepartmentService.createDepartment(department).then(function(msg) {
                    $("#addDepartment").modal("hide");
                    $scope.error_add_department="";
                    toastr.success('New department has been created with success');
                    refreshData();
                });
            }else{
                $scope.error_add_department="All fields are required";

            }

        };
        $scope.addCompany=function(company){
            if($scope.formAddCompany.$valid){
                CompanyService.createCompany(company).then(function(msg) {
                    $scope.companies.push(msg.data);
                    $("#addCompany").modal("hide");
                    $scope.error_add_company="";
                    toastr.success('New company has been created with success');

                });
            }else{
                $scope.error_add_company="company name is required";

            }

        };
        $scope.loadDepartments=function(company){
            $scope.selectedCompany=company;
            $scope.msgLoadDepartments="Loading............";
            toastr.info('Loading departments list.....');
            DepartmentService.getDepartments(company.id).then(function(msg){
                $scope.departments=msg.data;
                $scope.msgLoadDepartments="";});

        };
        $scope.loadEmployees=function(dep){
            $scope.selectedDep=dep;
            $scope.msgLoadWorkers="Loading............";
            toastr.info('Loading workers list.....');
            EmployeService.getEmployees(dep.id).then(function(msg){
                $scope.workers=msg.data;
                $scope.msgLoadWorkers="";

            });


        };
        $scope.showWorker=function(worker){
            $scope.selectedWorker=worker;
            $('.work').popover();
        };
        $scope.logout=function(){
            LoginService.logout();

        };
        function initDeleteCompany(){
            $("#modalDeleteCompany").modal("show");
        }
        $scope.initRemoveWorker=function(){
            if($scope.selectedWorker!=null){
                $("#modalDeleteWorker").modal("show");
            }else {

                toastr.warning('Select an employee first to remove it !!');
            }

        }

        $scope.initRemoveDepartment=function(){
            if($scope.selectedDep!=null){
                $("#modalDeleteDepartment").modal("show");
            }else {

                toastr.warning('Select a department first to remove it !!');
            }

        };
        $scope.removeCompany=function(){
            if($scope.selectedCompany!=null){
                initDeleteCompany();
            }else {

                toastr.warning('Select a company first to remove it !!');
            }

        };



    };
    PeopleController.$inject=['$scope','LoginService','CompanyService','DepartmentService','EmployeService'];

    angular.module("HrData").controller("PeopleController",PeopleController);



})();