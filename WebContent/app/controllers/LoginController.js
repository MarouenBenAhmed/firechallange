/**
 * Created by MBM info on 04/04/2015.
 */
(function(){

    var LoginController=function($scope,LoginService){
        /**
         * Init function allow to init data into $scope
         * By defaults Values
         *
         */
        function init(){
           $scope.admin={
               username:'',
               password:''

           };
           $scope.error_msg="";
            $scope.submitted = false;
       }
        init();
        $scope.login=function(admin){
            $scope.submitted = true;
            if($scope.form.$valid){
                LoginService.login($scope,admin);
            }else{

                $scope.error_msg="All feilds (*) are required ";

            }

        }

    };
    LoginController.$inject=['$scope','LoginService'];

    angular.module("HrData").controller("LoginController",LoginController);



})();