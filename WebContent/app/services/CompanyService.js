/**
 * Created by MBM info on 06/04/2015.
 */
/**
 * Created by MBM info on 05/04/2015.
 */
(function(){
    var CompanyService=function($http){
        this.getCompanies=function(){

       return $http.get("HrData/company/all");

        };
        this.createCompany=function(company){
            return $http.post("HrData/company/new",company);

        };
        this.removeCompany=function(company){

            return $http.post("HrData/company/remove",company);
        };

    }
    CompanyService.$inject=["$http"];
    angular.module("HrData").service("CompanyService",CompanyService)


})();