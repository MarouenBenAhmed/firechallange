/**
 * Created by MBM info on 06/04/2015.
 */
/**
 * Created by MBM info on 06/04/2015.
 */
/**
 * Created by MBM info on 05/04/2015.
 */
(function(){
    var DepartmentService=function($http){
        this.getDepartments=function(company_id){

            return $http.get("HrData/department/company_id="+company_id

            );
        };
        this.createDepartment=function(department){

           return $http.post("HrData/department/new",department);
        };
        this.removeDepartment=function(department){
            return $http.post("HrData/department/remove",department);

        };

    }
    DepartmentService.$inject=["$http"];
    angular.module("HrData").service("DepartmentService",DepartmentService)


})();