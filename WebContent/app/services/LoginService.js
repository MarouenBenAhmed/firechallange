/**
 * Created by MBM info on 05/04/2015.
 */
(function(){
var LoginService=function(SessionService,$http,$location){
    this.login=function(scope,data){
 var $promise=$http.post('HrData/autho/login',data);
        $promise.then(function (msg) {

            if(msg.data.success){
          SessionService.set('user',msg.data.uid);
                $location.path('/people_directory');
           } else{
               scope.error_msg=msg.data.error_msg;
               $location.path("/");

           }
        })


    };
    this.logout=function(){
      var $promise=$http.post('HrData/autho/logout');
        $promise.then(function (msg) {
            SessionService.destroy('user');
            $location.path("/");
        })

    };
    this.isLogged=function(){
        if(SessionService.get("user")) {
           return true;
       }
        return false;
    };

}
    LoginService.$inject=["SessionService","$http","$location"];
angular.module("HrData").service("LoginService",LoginService)


})();