/**
 * Created by MBM info on 06/04/2015.
 */
/**
 * Created by MBM info on 05/04/2015.
 */
(function(){
    var SessionService=function(){

        this.set=function(key,value){

            sessionStorage.setItem(key,value);
        };
        this.get=function(key){

           return sessionStorage.getItem(key);
        };
        this.destroy=function(key){
            sessionStorage.removeItem(key)

        }

    }
    angular.module("HrData").service("SessionService",SessionService)


})();