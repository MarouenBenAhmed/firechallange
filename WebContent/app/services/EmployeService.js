/**
 * Created by MBM info on 07/04/2015.
 */

(function(){
    var EmployeService=function($http){

        this.getEmployees=function(department_id){

            return $http.get("HrData/employee/department_id="+department_id);

        };
        this.createWorker=function(woker){
            return $http.post("HrData/employee/new",woker);
        };
        this.removeWorker=function(woker){
            return $http.post("HrData/employee/remove",woker);
        };
    }
    EmployeService.$inject=["$http"];
    angular.module("HrData").service("EmployeService",EmployeService)


})();